/*
**  OSSP val - Value Access
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**
**  This file is part of OSSP val, a value access library which
**  can be found at http://www.ossp.org/pkg/lib/val/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  val_test.c: test suite
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#if defined(HAVE_DMALLOC_H) && defined(WITH_DMALLOC)
#include "dmalloc.h"
#endif
#include "val.h"

static void die(char *msg)
{
    fprintf(stderr, "ERROR: %s\n", msg);
    exit(-1);
}

static val_rc_t dumper(void *ctx, const char *name, int type, const char *desc, void *data)
{
    switch (type) {
        case VAL_TYPE_VAL:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_VAL,    desc=<%20s>, data@%.8lx INTERNAL\n",
                    (char *)ctx, name,             desc, (long)data);
            break;
        case VAL_TYPE_PTR:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_PTR,    desc=<%20s>, data@%.8lx=%.8lx\n",
                    (char *)ctx, name,             desc, (long)data, *(long *)data);
            break;
        case VAL_TYPE_CHAR:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_CHAR,   desc=<%20s>, data@%.8lx='%c'\n",
                    (char *)ctx, name,             desc, (long)data, *(char *)data);
            break;
        case VAL_TYPE_SHORT:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_SHORT,  desc=<%20s>, data@%.8lx=%8d\n",
                    (char *)ctx, name,             desc, (long)data, *(short *)data);
            break;
        case VAL_TYPE_INT:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_INT,    desc=<%20s>, data@%.8lx=%8d\n",
                    (char *)ctx, name,             desc, (long)data, *(int *)data);
            break;
        case VAL_TYPE_LONG:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_LONG,   desc=<%20s>, data@%.8lx=%8ld\n",
                    (char *)ctx, name,             desc, (long)data, *(long *)data);
            break;
        case VAL_TYPE_FLOAT:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_FLOAT,  desc=<%20s>, data@%.8lx=%8f\n",
                    (char *)ctx, name,             desc, (long)data, *(float *)data);
            break;
        case VAL_TYPE_DOUBLE:
            printf("DEBUG: <%10s>, name=<%10s>, VAL_TYPE_DOUBLE, desc=<%20s>, data@%.8lx=%8f\n",
                    (char *)ctx, name,             desc, (long)data, *(double *)data);
            break;
        default:
            printf("DEBUG: <%10s>, name=<%10s>, type = %.8lx,    desc=<%20s>, data@%.8lx\n",
                    (char *)ctx, name, (long)type, desc, (long)data);
    }
    return VAL_OK;
}

int main(int argc, char *argv[])
{
    val_rc_t rc;
    val_t *v1, *v2, *v3;
    int testint;
    int testintout;

    /* initialize variables */
    testint    = 10;
    testintout = !testint;
    if (testint == testintout)
        die("testint/testintout initialization");
    printf("DEBUG: testint@%.8lx, testintout@%.8lx\n", (long)&testint, (long)&testintout);

    /* create first val_t */
    if ((rc = val_create(&v1)) != VAL_OK)
        die("val_create() #1");

    /* register variable under first val_t and name it "foo" */
    if ((rc = val_reg(v1, "foo", VAL_TYPE_INT, "foo variable", (void *)&testint)) != VAL_OK)
        die("val_reg() #1");

    /* work with testint */
    testint++;

    /* get the value of testint aka "foo" into testintout */
    if ((rc = val_get(v1, "foo", &testintout)) != VAL_OK)
        die("val_get() #1");
    if (testint != testintout)
        die("testint/testintout compare #1");

    /* set the value of testint aka "foo" using name */
    if ((rc = val_set(v1, "foo", 2002)) != VAL_OK)
        die("val_set() #1");

    /* get the value of testint aka "foo" into testintout */
    if ((rc = val_get(v1, "foo", &testintout)) != VAL_OK)
        die("val_get() #2");

    /* comparison */
    if (testint != testintout)
        die("testint/testintout not equal after setting testint by name");

    /* create a second val_t */
    if ((rc = val_create(&v2)) != VAL_OK)
        die("val_create() #2");

    /* register second val_t as child of first and name it "bar" - reg method */
    if ((rc = val_reg(v1, "bar", VAL_TYPE_VAL, "bar child", (void *)&v2)) != VAL_OK)
        die("val_reg() #2");

    /* create a third val_t */
    if ((rc = val_create(&v3)) != VAL_OK)
        die("val_create() #3");

    /* register third val_t as child of first and name it "baz" - set method */
    if ((rc = val_reg(v1, "baz", VAL_TYPE_VAL, "baz child", NULL)) != VAL_OK)
        die("val_reg() #3");
    if ((rc = val_set(v1, "baz", v3)) != VAL_OK)
        die("val_set() #2");

    /* register variable under second val_t and name it "quux2" - direct method */
    if ((rc = val_reg(v2, "quux2", VAL_TYPE_INT, "quux2 variable", (void *)&testint)) != VAL_OK)
        die("val_reg() #4");

    /* set the value of testint using name - direct method */
    if ((rc = val_set(v2, "quux2", testint+1)) != VAL_OK)
        die("val_set() #3");

    /* work with testint */
    testint++;

    /* get the value of testint aka "quux2" into testintout - direct method */
    if ((rc = val_get(v2, "quux2", &testintout)) != VAL_OK)
        die("val_get() #3");

    /* comparison */
    if (testint != testintout)
        die("testint/testintout compare #2");

    /* register variable under second val_t and name it "quux3" - OO method */
    if ((rc = val_reg(v1, "baz.quux3", VAL_TYPE_INT, "quux3 variable", (void *)&testint)) != VAL_OK)
        die("val_reg() #5");

    /* set the value of testint using name - OO method */
    if ((rc = val_set(v1, "baz.quux3", testint+1)) != VAL_OK)
        die("val_set() #4");

    /* work with testint */
    testint++;

    /* get the value of testint aka "quux3" into testintout - OO method */
    if ((rc = val_get(v1, "baz.quux3", &testintout)) != VAL_OK)
        die("val_get() #4");

    /* comparison */
    if (testint != testintout)
        die("testint/testintout compare #3");

    /* work with testintout2 */
    testintout--;

    /* set every data type - OO method */
    if ((rc = val_reg(v1, "bar.ptr", VAL_TYPE_PTR, "bar ptr (&testint)", NULL)) != VAL_OK)
        die("val_ret  for bar.ptr using inline data");
    if ((rc = val_set(v1, "bar.ptr", &testint)) != VAL_OK)
        die("val_set  for bar.ptr using inline data");
    if ((rc = val_reg(v1, "bar.char", VAL_TYPE_CHAR, "bar character (!)", NULL)) != VAL_OK)
        die("val_ret  for bar.char using inline data");
    if ((rc = val_set(v1, "bar.char", '!')) != VAL_OK)
        die("val_set  for bar.char using inline data");
    if ((rc = val_reg(v1, "bar.short", VAL_TYPE_SHORT, "bar short (555)", NULL)) != VAL_OK)
        die("val_ret  for bar.short using inline data");
    if ((rc = val_set(v1, "bar.short", 555)) != VAL_OK)
        die("val_set  for bar.short using inline data");
    if ((rc = val_reg(v1, "bar.int", VAL_TYPE_INT, "bar integer (76543)", NULL)) != VAL_OK)
        die("val_ret  for bar.int using inline data");
    if ((rc = val_set(v1, "bar.int", 76543)) != VAL_OK)
        die("val_set  for bar.int using inline data");
    if ((rc = val_reg(v1, "bar.long", VAL_TYPE_LONG, "bar long (2097152)", NULL)) != VAL_OK)
        die("val_ret  for bar.long using inline data");
    if ((rc = val_set(v1, "bar.long", 2097152)) != VAL_OK)
        die("val_set  for bar.long using inline data");
    if ((rc = val_reg(v1, "bar.float", VAL_TYPE_FLOAT, "bar float (1.955830)", NULL)) != VAL_OK)
        die("val_ret  for bar.float using inline data");
    if ((rc = val_set(v1, "bar.float", 1.95583)) != VAL_OK)
        die("val_set  for bar.float using inline data");
    if ((rc = val_reg(v1, "bar.double", VAL_TYPE_DOUBLE, "bar double (3.1415+)", NULL)) != VAL_OK)
        die("val_ret  for bar.double using inline data");
    if ((rc = val_set(v1, "bar.double", 3.14159265358979)) != VAL_OK)
        die("val_set  for bar.double using inline data");

    /* apply data dumper for everything up to nine levels deep */
    val_apply(v1, "", 9, dumper, "all" );

    /* apply data dumper for upper level only */
    val_apply(v1, "", 0, dumper, "upper" );

    /* apply data dumper for everything below baz - direct method */
    val_apply(v3, "", 9, dumper, "baz-dir");

    /* apply data dumper for everything below baz - OO method */
    val_apply(v1, "baz", 9, dumper, "baz- OO");

    /* apply data dumper for bar.char only - direct method */
    val_apply(v2, "char", 0, dumper, "char" );

    /* apply data dumper for bar.char only - OO method */
    val_apply(v1, "bar.char", 0, dumper, "bar.char" );

    /* destroy val_t and free memory */
    if ((rc = val_destroy(v3)) != VAL_OK)
        die("val_destroy #1");

    /* destroy val_t and free memory */
    if ((rc = val_destroy(v2)) != VAL_OK)
        die("val_destroy #2");

    /* destroy val_t and free memory */
    if ((rc = val_destroy(v1)) != VAL_OK)
        die("val_destroy #3");

    return 0;
}

